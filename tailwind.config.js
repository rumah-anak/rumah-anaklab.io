module.exports = {
  prefix: '',
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./app.vue",
    "./error.vue",
  ],
  important: false,
  separator: ':',
  theme: {},
  variants: {},
  plugins: []
}
