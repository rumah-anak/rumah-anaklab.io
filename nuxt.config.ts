import path from 'path'
   
export default defineNuxtConfig({
  devtools: { enabled: true },
  css: ['@/assets/scss/tailwind.scss'],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  modules: [

    '@vite-pwa/nuxt',
    '@nuxtjs/tailwindcss',
    '@nuxt/image',
    'nuxt-icons',
  ],
  dir: {
    public: 'static',
  },
  nitro: {
    output: {
      publicDir: 'public',
    },
  },
  pwa: {
    manifest: {
      name: 'The Pita',
      short_name: 'The Pita',
      description: 'This is description for PWA apps',
      lang: 'en',
      start_url: '/',
      display: 'standalone',
      background_color: '#FFF6F6',
      theme_color: '#000000',
      icons: [
        {
          src: '/icons/Small-Logo-ThePita.png',
          sizes: '192x192',
          type: 'image/png',
        },
      ],
    },
    devOptions: {
      enabled: true,
      type: 'module',
    },
  },
})
